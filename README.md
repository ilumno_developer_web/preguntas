### Ilumno Prueba de Conceptos
![](https://www.ilumno.com/assets/icons/apple-icon-72x72.png)

**1.	Que diferencia existe entre una pagina AMP y una página HTML**

La principal diferencia entre una página AMP y una página HTML es que las páginas AMP están diseñadas para cargar rápidamente en dispositivos móviles y mejorar la velocidad de carga de la página. Para lograr esto, las páginas AMP utilizan una versión simplificada de HTML llamada AMP HTML, que tiene un conjunto reducido de etiquetas y atributos, y no permite el uso de JavaScript personalizado.

Otra diferencia importante es que las páginas AMP utilizan un conjunto de componentes AMP para mejorar la velocidad de carga. Estos componentes son elementos especiales de AMP HTML que se pueden utilizar para mejorar el rendimiento de la página, como imágenes y videos.

En resumen, las páginas AMP son una versión optimizada de las páginas HTML para mejorar la velocidad de carga y la experiencia de usuario en dispositivos móviles.

**2.	Que herramientas son las ideales para validar el SEO de una página Web**

> **Google Search Console**,
> **SEMrush**,
> **Ahrefs**, 
> **Google Lighthouse**, 
> **Yoast SEO**

**3.	Que diferencia encuentra entre un api REST y un API SOAP**

La principal diferencia entre una API REST y una API SOAP es la forma en que se estructuran y se envían los datos y las operaciones. Una API REST utiliza una arquitectura basada en recursos y se utiliza el protocolo HTTP para obtener y enviar los datos, mientras que una API SOAP utiliza mensajes XML y puede utilizar cualquier protocolo de red para enviarlos.

**4.	¿Qué es una inyección de dependencias?**

La inyección de dependencias es un patrón de diseño en el que un objeto recibe las dependencias que necesita de una fuente externa en lugar de crearlas por sí mismo. Esto permite que los objetos sean más fáciles de probar y mantener, ya que no dependen directamente de cómo se crean o proporcionan sus dependencias. En lugar de crear sus propias dependencias, un objeto simplemente recibe las dependencias que necesita como argumentos de entrada en su constructor o a través de un conjunto de métodos establecedores. Esto permite que los objetos sean más flexibles y fáciles de reutilizar en diferentes contextos.

**5.	Cual modulo de Drupal le parece apropiado para el manejo de SEO**

Módulo de URL amigables, Módulo de etiquetas META, Módulo de redirecciones, Módulo de Google Analytics

**6.	Cual plugin de WordPress le parece apropiado para una buena configuración de SEO**

Yoast SEO: es uno de los plugins de SEO más populares para WordPress, y ofrece una amplia gama de funciones para optimizar la SEO de un sitio, como la generación de metadatos, la optimización de títulos y descripciones, y la generación de sitemaps.

**7.	En donde se almacena la configuración de los módulos instalados de Drupal 8/9**

En Drupal 8/9, la configuración de los módulos instalados se almacena en la base de datos del sitio web. Drupal utiliza la tabla "config" de la base de datos para almacenar la configuración de todos los módulos y componentes del sistema

**8.	¿Qué significa arquitectura Headless, y como implementaría una con Drupal 9?**

La arquitectura "Headless" se refiere a la separación del "frontend" y el "backend" de una aplicación web o sistema. En una arquitectura Headless, el "backend" se encarga de gestionar y almacenar los datos y el "frontend" se encarga de mostrar esos datos al usuario a través de una interfaz de usuario.

**9.	¿Con que comando de Git se pueden guardar cambios en memoria, para ser recuperados más adelante?**

Para guardar tus cambios en memoria temporalmente, puedes utilizar el comando git stash. Este comando guarda tus cambios en un "almohadilla" temporal, sin hacer commit, y regresa a la última versión del código que se encuentra en tu rama actual

**10.	Mencione 5 buenas prácticas de SEO que haya implementado en algún proyecto**

Utilizar palabras clave relevantes: asegúrate de incluir palabras clave relevantes en el título de la página, el contenido y las etiquetas de encabezado.

Crear contenido de calidad: el contenido de calidad es un factor importante en el SEO, ya que los motores de búsqueda valoran la información útil y relevante para los usuarios.

Utilizar etiquetas de título y descripción: las etiquetas de título y descripción aparecen en los resultados de búsqueda y pueden ayudar a atraer a los usuarios a tu sitio. Asegúrate de incluir palabras clave relevantes en estas etiquetas.

Enlazar a otras páginas y sitios: los enlaces entrantes (de otros sitios hacia el tuyo) son un factor importante en el SEO, ya que muestran a los motores de búsqueda que tu sitio es valioso y relevante.

Utilizar URLs amigables: las URLs amigables son fáciles de leer y recordar, lo que las hace más atractivas para los usuarios y mejores para el SEO. Utiliza palabras clave relevantes en las URLs de tus páginas.





***BONUS (Se tendrá en cuenta, no es obligatorio)
Explique cómo implementaría una arquitectura de CI/CD desde GItlab o Github, a nivel general, como seria el manejo de ramas.***

-	Configurar el repositorio de código, estableciendo ramas, permisos y etiquetas.
-	Configurar el flujo de trabajo de CI/CD en el repositorio de código, incluyendo tareas como compilar código, ejecutar pruebas unitarias y realizar pruebas de integración.
-	Integrar herramientas de terceros para realizar tareas específicas.
-	Configurar el proceso de despliegue para entregar el código a producción.

En el manejo de ramas, es importante tener en cuenta que se utilizan para separar diferentes versiones del código y fusionarlas cuando estén listas para ser liberadas. 

